<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder        
            ->add('id_user', HiddenType::class,[
                'label' => 'id_user',
                'attr'=> [
                    'class' => 'form-control', 
                    'empty_data' => uniqid(),
                    'required' => false 
                ],
            ])
            ->add('name', TextType::class,[
                'label' => 'Nombre',
                'attr'=> [
                    'class' => 'form-control', 
                    'placeholder' => 'Nombre', 
                    'required' => true, 
                    'autocomplete' => 'off'
                ],
            ])
            ->add('lastname', TextType::class,[
                'label' => 'Apellidos',
                'attr'=> [
                    'class' => 'form-control', 
                    'placeholder' => 'Apellidos', 
                    'required' => true, 
                    'autocomplete' => 'off'
                ],
            ])
            ->add('email', EmailType::class,[
                'label' => 'Email',
                'attr'=> [
                    'class' => 'form-control', 
                    'placeholder' => 'Email', 
                    'required' => true, 
                    'autocomplete' => 'off'
                ],
            ])
            ->add('password', PasswordType::class,[
                'label' => 'password',
                'attr'=> [
                    'class' => 'form-control', 
                    'placeholder' => 'password', 
                    'required' => true, 
                    'autocomplete' => 'off'
                ],
            ])
            ->add('image', TextType::class,[
                'label' => 'Image',
                'required' => false, 
                'empty_data' => 'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png',
                'attr'=> [
                    'class' => 'form-control', 
                    'placeholder' => 'Image',                     
                    'autocomplete' => 'off'
                ],
            ])
            ->add('submit',SubmitType::class,[
                'label' => 'Guardar',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
