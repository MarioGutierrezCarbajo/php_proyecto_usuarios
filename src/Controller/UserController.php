<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;
use App\Form\UsersType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class UserController extends AbstractController
{    
    public function getUsers(EntityManagerInterface $doctrine): Response
    {
        $repository = $doctrine->getRepository(User::class);
        $listUsers = $repository->findAll();
        return $this->render('user/user.html.twig',[
            'listUsers' => $listUsers
        ]);
    }

    public function createUser(Request $request, EntityManagerInterface $doctrine): Response
    {
        $user = new User();

        $form_user = $this->createForm(UsersType::class, $user);
        $form_user->handleRequest($request);

        if ($form_user->isSubmitted() && $form_user->isValid()){
            $user = $form_user->getData();
            $user->setStatus(1);
            $doctrine->persist($user);
            $doctrine->flush();

            return $this->redirectToRoute('getUsers');
        }

        return $this->render('user/user_create.html.twig',[
            'form_user' => $form_user->createView()
        ]);
        //https://media-exp2.licdn.com/dms/image/C4D03AQF1w1A8-adHrA/profile-displayphoto-shrink_400_400/0/1617806179272?e=1663804800&v=beta&t=7qKrY01rY6yDyWNWTsogg9iHGy0N3WV4aZvswx4p5vE
    }

    public function deleteUser($id, EntityManagerInterface $doctrine){
        $repository = $doctrine->getRepository(User::class);
        $user = $repository->find($id);

        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success', 'usuario borrado correctamente');
        return $this->redirectToRoute('getUsers');
    }

    public function updateUser(Request $request, EntityManagerInterface $doctrine, $id)
    {
        $repository = $doctrine->getRepository(User::class);
        $user = $repository->find($id);
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $doctrine->persist($user);
            $doctrine->flush();
            $this->addFlash('success', 'user modificado correctamente');
            return $this->redirectToRoute('getUsers');
        }
        
        return $this->render('user/user_create.html.twig',[
            'form_user' => $form->createView()
        ]);
    }
}
